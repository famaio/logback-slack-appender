package com.github.maricn.logback;

import static org.junit.Assert.*;

public class SlackAppenderTest {
    private static Logger logger = LoggerFactory.getLogger(SlackAppenderTest.class);

    @Test
    public void testSomething() {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
        final Appender mockAppender = mock(Appender.class);
        when(mockAppender.getName()).thenReturn("MOCK");
        root.addAppender(mockAppender);

        //... do whatever you need to trigger the log

        verify(mockAppender).doAppend(argThat(new ArgumentMatcher() {
            @Override
            public boolean matches(final Object argument) {
                return ((LoggingEvent)argument).getFormattedMessage().contains("Hey this is the message I want to see");
            }
        }));
    }
}